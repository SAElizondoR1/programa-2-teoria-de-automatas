package TA_032_EA2_E3;

public class AnalizadorSintactico {
    private final AnalizadorLexico analizadorLexico;

    public AnalizadorSintactico(String texto) {
        analizadorLexico = new AnalizadorLexico(texto);
    }

    public boolean cadenaValida() {
        try {
            return analizarEstadoS();
        }
        catch (Exception excepcion) {
            return false;
        }
    }

    private boolean analizarEstadoS() throws Exception {

        if(analizadorLexico.doesntStartWith('i'))
            throw new ExcepcionComponenteInvalido("No empieza con i");
        if(analizadorLexico.doesntEndWith('j'))
            throw new ExcepcionComponenteInvalido("No termina con j");

        analizadorLexico.substring('i', 'j');

        if(analizadorLexico.doesntEndWith('j'))
            throw new ExcepcionComponenteInvalido("No termina con doble j");

        analizadorLexico.substring('v', 'j');

        return analizarEstadoA();
    }

    private boolean analizarEstadoA() throws Exception {

        if(analizadorLexico.doesntStartWith('w'))
            throw new ExcepcionComponenteInvalido("No empieza con w");
        if(analizadorLexico.doesntEndWith('r'))
            throw new ExcepcionComponenteInvalido("No termina con r (w')");

        analizadorLexico.substring('w', 'r');

        if(analizadorLexico.doesntEndWith('r'))
            throw new ExcepcionComponenteInvalido("No termina con doble r (w')");

        analizadorLexico.substring('v', 'r');

        if(analizadorLexico.stringEquals('i'))
            return true;
        else
            return analizarEstadoA();
    }
}
