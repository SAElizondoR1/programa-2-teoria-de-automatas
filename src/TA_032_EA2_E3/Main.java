package TA_032_EA2_E3;

import java.util.Scanner;

public class Main {
    static Scanner escaner = new Scanner(System.in);

    public static void main(String[] args) {
	    String entrada; // cadena de entrada
        mensajeInicio();

        do {
            System.out.println("Introduzca una cadena de caracteres: ");
            entrada = escaner.nextLine();
            if (esValidaParaGramatica(entrada))
                System.out.println("La cadena es válida para la gramática.");
            else
                System.out.println("La cadena NO es válida para la gramática.");
        } while (analizarOtra());
    }

    public static void mensajeInicio() {
        System.out.println("Evidencia de Aprendizaje 2 - Teoría de Autómatas\n");
        System.out.println("Programa que indica si una cadena es válida para la gramática regular " +
                "de la forma)");
        System.out.println("L = {i (w)^n i (w')^(2n) j^2}\n");
        System.out.println("donde:");
        System.out.println("i = 1907852");
        System.out.println("w = er");
        System.out.println("w' = re");
        System.out.println("j = sergio");
        System.out.println("n >= 1\n");
    }

    public static boolean analizarOtra() {
        System.out.println("¿Desea analizar otra cadena? (s - Sí, n - No)");
        String respuesta = escaner.nextLine();
        if (respuesta.equalsIgnoreCase("s"))
            return true;
        else if (respuesta.equalsIgnoreCase("n"))
            return false;

        System.out.println("Entrada incorrecta");
        return analizarOtra();
    }

    public static boolean esValidaParaGramatica(String entrada) {
        // w = "er";     Apellidos: Elizondo Rodríguez
        // wi = "re";    w inversa
        // i = "1907852";    Matrícula: 1907852
        // j = "sergio";     Primer nombre: Sergio

        AnalizadorSintactico analizadorSintactico = new AnalizadorSintactico(entrada);
        return analizadorSintactico.cadenaValida();
    }
}
