package TA_032_EA2_E3;

public class AnalizadorLexico {
    private String texto;

    public AnalizadorLexico(String texto) {
        this.texto = texto;
    }

    //i->matricula
    //j->primer nombre
    //w->iniciales apellido
    //r->iniciales apellido inverso

    public boolean doesntStartWith(char tipo)
    {
        return !switch (tipo) {
            case 'i' -> texto.startsWith("1907852");
            case 'j' -> texto.startsWith("sergio");
            case 'w' -> texto.startsWith("er");
            case 'r' -> texto.startsWith("re");
            default -> false;
        };
    }

    public boolean doesntEndWith(char tipo)
    {
        return !switch (tipo) {
            case 'i' -> texto.endsWith("1907852");
            case 'j' -> texto.endsWith("sergio");
            case 'w' -> texto.endsWith("er");
            case 'r' -> texto.endsWith("re");
            default -> false;
        };
    }

    public boolean stringEquals(char tipo)
    {
        return switch (tipo) {
            case 'i' -> texto.equals("1907852");
            case 'j' -> texto.equals("sergio");
            case 'w' -> texto.equals("er");
            case 'r' -> texto.equals("re");
            default -> false;
        };
    }

    //v de void, por si se necesita hacer un substring de solo un lado
    public int getTipoLenght(char tipo)
    {
        return switch (tipo) {
            case 'i' -> 7;
            case 'j' -> 6;
            case 'w', 'r' -> 2;
            default -> 0;
        };
    }

    public void substring(char tipoInicio, char tipoFinal)
    {
        int slenght = getTipoLenght(tipoInicio);
        int elenght = getTipoLenght(tipoFinal);
        texto = texto.substring(slenght, texto.length() - elenght);
    }
}